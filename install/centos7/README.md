```
Distribution      : CentOS 7 Minimal
GitLab version    : 7.3-stable
Web Server        : Nginx
Init system       : sysvinit
Database          : MySQL
Contributors      : @nielsbasjes, @axilleas, @mairin, @ponsjuh, @yorn, @psftw, @etcet, @mdirkse, @nszceta, @AxelPanda
Additional Notes  : In order to get a proper Ruby setup we build it from source, and a new source for users in China Mainland
```

## Overview

Please read [requirements.md](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/install/requirements.md) for hardware and platform requirements.

### Important Notes

The following steps have been known to work and should be followed from up to bottom.
If you deviate from this guide, do it with caution and make sure you don't violate
any assumptions GitLab makes about its environment. This instruction HASN'T been tried
on RHEL series.

**This guide assumes that you run every command as root.**

#### If you find a bug

If you find a bug/error in this guide please submit an issue or a Merge Request
following the contribution guide (see [CONTRIBUTING.md](https://gitlab.com/gitlab-org/gitlab-recipes/blob/master/CONTRIBUTING.md)).

#### Security

Many setup guides of Linux software simply state: "disable selinux and firewall".
This guide does not disable any of them, we simply configure them as they were intended.
[Stop disabling SELinux](http://stopdisablingselinux.com/).

- - -

The GitLab installation consists of setting up the following components:

1. Install the base operating system (CentOS 7 Minimal) and Packages / Dependencies
2. Ruby
3. System Users
4. Database
5. GitLab
6. GitLab shell
7. Web server
8. Firewall

----------

## 1. Installing the operating system (CentOS 7 Minimal)

We start with a completely clean CentOS 7 "minimal" installation which can be
accomplished by downloading the appropriate installation iso file. Just boot the
system of the iso file and install the system.

Note that during the installation you use the *"Configure Network"* option (it's a
button in the same screen where you specify the hostname) to enable the *"Connect automatically"*
option for the network interface and hand (usually eth0).

**If you forget this option the network will NOT start at boot.**

The end result is a bare minimum CentOS installation that effectively only has
network connectivity and (almost) no services at all.

## Updating and adding basic software and services

### Install the required tools for GitLab

    yum -y update
    yum -y groupinstall 'Development Tools'
    yum -y install readline readline-devel ncurses-devel gdbm-devel glibc-devel tcl-devel openssl-devel curl-devel expat-devel db4-devel byacc sqlite-devel libyaml libyaml-devel libffi libffi-devel libxml2 libxml2-devel libxslt libxslt-devel libicu libicu-devel system-config-firewall-tui ruby git sudo wget crontabs logwatch logrotate perl-Time-HiRes

**Note:**
During this installation some files will need to be edited manually.
If you are familiar with vim set it as default editor with the commands below.
If you are not familiar with vim please skip this and keep using the default editor.

    # Install vim and set as default editor
    yum -y install vim-enhanced
    update-alternatives --set editor /usr/bin/vim.basic

    # For reStructuredText markup language support, install required package:
    yum -y install python-docutils

### Install Redis
Go to http://www.redis.io/download to download the source code for Redis, and follow the instruction on the download page.

    wget http://download.redis.io/releases/redis-2.8.17.tar.gz
    tar xzf redis-2.8.17.tar.gz
    cd redis-2.8.17
    make

Note: If some errors occur during compilation, execute the command below.

    make test

After finishing compilation, install Redis.

    make install
    ./utils/install_server.sh

### Configure Redis
Create /etc/init.d/redis and use the following code as an init script.

    nano /etc/init.d/redis

    ###########################
    PATH=/usr/local/bin:/sbin:/usr/bin:/bin
     
    REDISPORT=6379
    EXEC=/usr/local/bin/redis-server
    REDIS_CLI=/usr/local/bin/redis-cli
     
    PIDFILE=/var/run/redis.pid
    CONF="/etc/redis/6379.conf"
     
    case "$1" in
        start)
            if [ -f $PIDFILE ]
            then
                    echo "$PIDFILE exists, process is already running or crashed"
            else
                    echo "Starting Redis server..."
                    $EXEC $CONF
            fi
            if [ "$?"="0" ]
            then
                  echo "Redis is running..."
            fi
            ;;
        stop)
            if [ ! -f $PIDFILE ]
            then
                    echo "$PIDFILE does not exist, process is not running"
            else
                    PID=$(cat $PIDFILE)
                    echo "Stopping ..."
                    $REDIS_CLI -p $REDISPORT SHUTDOWN
                    while [ -x ${PIDFILE} ]
                   do
                        echo "Waiting for Redis to shutdown ..."
                        sleep 1
                    done
                    echo "Redis stopped"
            fi
            ;;
       restart|force-reload)
            ${0} stop
            ${0} start
            ;;
      *)
        echo "Usage: /etc/init.d/redis {start|stop|restart|force-reload}" >&2
            exit 1
    esac
    ##############################

Save and grant execution.

    chmod +x /etc/init.d/redis

Make sure redis is started on boot:

    nano /etc/rc.d/rc.local

    service redis start # append this to the end of the file

Start Redis

    service redis start

### Install mail server

In order to receive mail notifications, make sure to install a
mail server. The recommended one is postfix and you can install it with:

    yum -y install postfix

To use and configure sendmail instead of postfix see [Advanced Email Configurations](e-mail/configure_email.md).

### Configure the default editor

You can choose between editors such as nano, vi, vim, etc.
In this case we will use vim as the default editor for consistency.

    ln -s /usr/bin/vim /usr/bin/editor

To remove this alias in the future:

    rm -i /usr/bin/editor


### Install Git from Source (optional)

Make sure Git is version 1.7.10 or higher, for example 1.7.12 or 1.8.4

    git --version

If not, install it from source. First remove the system Git:

    yum -y remove git

Install the pre-requisite files for Git compilation:

    yum install zlib-devel perl-CPAN gettext curl-devel expat-devel gettext-devel openssl-devel

Download and extract it:

    mkdir /tmp/git && cd /tmp/git
    curl --progress https://www.kernel.org/pub/software/scm/git/git-2.0.0.tar.gz | tar xz
    cd git-2.0.0/
    ./configure
    make
    make prefix=/usr/local install

Make sure Git is in your `$PATH`:

    which git

You might have to logout and login again for the `$PATH` to take effect.
**Note:** When editing `config/gitlab.yml` (step 5), change the git `bin_path` to `/usr/local/bin/git`.

----------

## 2. Ruby

The use of ruby version managers such as [RVM](http://rvm.io/), [rbenv](https://github.com/sstephenson/rbenv) or [chruby](https://github.com/postmodern/chruby) with GitLab in production frequently leads to hard to diagnose problems. Version managers are not supported and we strongly advise everyone to follow the instructions below to use a system ruby.

Remove the old Ruby 1.8 package if present. GitLab only supports the Ruby 2.0+ release series:

    yum remove ruby

Remove any other Ruby build if it is still present:

    cd <your-ruby-source-path>
    make uninstall

Download Ruby and compile it:

    mkdir /tmp/ruby && cd /tmp/ruby
    curl --progress ftp://ftp.ruby-lang.org/pub/ruby/2.1/ruby-2.1.2.tar.gz | tar xz
    cd ruby-2.1.2
    ./configure --disable-install-rdoc
    make
    make prefix=/usr/local install

Install the Bundler Gem:

    gem install bundler --no-doc

Logout and login again for the `$PATH` to take effect. Check that ruby is properly
installed with:

    which ruby
    # /usr/local/bin/ruby
    ruby -v
    # ruby 2.1.2p95 (2014-05-08 revision 45877) [x86_64-linux]

----------

## 3. System Users

Create a `git` user for Gitlab:

    adduser --system --shell /bin/bash --comment 'GitLab' --create-home --home-dir /home/git/ git

**Important:** In order to include `/usr/local/bin` to git user's PATH, one way is to edit the sudoers file. As root run:

    visudo

Then search for this line:

    Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin

and append `/usr/local/bin` like so:

    Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin

Save and exit.


----------

## 4. Database

### 4.1 MySQL

Because of some obvious reasons, MySQL is nolonger included in the repos in CentOS 7 and MariaDB cannot work together with MySQL.
Remove MariaDB first and then install MySQL.
Check all the installed mariadb packages.

    rpm -qa | grep mariadb

Remove them all.

    rpm -e --nodeps mariadb-*

Download and install `mysql`:

    wget http://dev.mysql.com/get/Downloads/MySQL-5.6/MySQL-5.6.21-1.el7.x86_64.rpm-bundle.tar
    tar -xvf MySQL-5.6.21-1.el7.x86_64.rpm-bundle.tar
    rpm -ivh MySQL-server-5.6.21-1.el7.x86_64.rpm
    rpm -ivh MySQL-shared-5.6.21-1.el7.x86_64.rpm
    rpm -ivh MySQL-client-5.6.21-1.el7.x86_64.rpm
    rpm -ivh MySQL-devel-5.6.21-1.el7.x86_64.rpm

Copy the configuration file and MySQL files, grant privileges.

    cp /usr/share/mysql/my-default.cnf /etc/my.cnf
    mv /var/lib/mysql　/home/mysql/data/
    chown -R mysql:mysql /home/mysql/data
    chmod -R 755 /home/mysql/data

Start MySQL and enable MySQL start on boot:

    service mysql start
    chkconfig mysql on

Set password for root user:

    mysqladmin -u root password

Login to MySQL (type the database root password):

    mysql -u root -p


Create a user for GitLab (change $password in the command below to a real password you pick):

    CREATE USER 'git'@'localhost' IDENTIFIED BY '$password';

Ensure you can use the InnoDB engine which is necessary to support long indexes.
If this fails, check your MySQL config files (e.g. `/etc/mysql/*.cnf`, `/etc/mysql/conf.d/*`) for the setting "innodb = off".

    SET storage_engine=INNODB;

Create the GitLab production database:

    CREATE DATABASE IF NOT EXISTS `gitlabhq_production` DEFAULT CHARACTER SET `utf8` COLLATE `utf8_unicode_ci`;

Grant the GitLab user necessary permissions on the table:

    GRANT SELECT, LOCK TABLES, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER ON `gitlabhq_production`.* TO 'git'@'localhost';

Quit the database session:

    \q

Try connecting to the new database with the new user:

    sudo -u git -H mysql -u git -p -D gitlabhq_production

Type the password you replaced $password with earlier.
Quit the database session:

    \q

----------
## 5. GitLab

    # We'll install GitLab into home directory of the user "git"
    cd /home/git

### Clone the Source

    # Clone GitLab repository
    sudo -u git -H git clone https://gitlab.com/gitlab-org/gitlab-ce.git -b 7-3-stable gitlab

**Note:** You can change `7-3-stable` to `master` if you want the *bleeding edge* version, but do so with caution!

### Configure it

    cd /home/git/gitlab

    # Copy the example GitLab config
    sudo -u git -H cp config/gitlab.yml.example config/gitlab.yml

    # Make sure to change "localhost" to the fully-qualified domain name of your
    # host serving GitLab where necessary
    #
    # If you want to use https make sure that you set `https` to `true`. See #using-https for all necessary details.
    #
    # If you installed Git from source, change the git bin_path to /usr/local/bin/git
    sudo -u git -H editor config/gitlab.yml

    # Make sure GitLab can write to the log/ and tmp/ directories
    chown -R git {log,tmp}
    chmod -R u+rwX  {log,tmp}

    # Create directory for satellites
    sudo -u git -H mkdir /home/git/gitlab-satellites
    chmod u+rwx,g+rx,o-rwx /home/git/gitlab-satellites

    # Make sure GitLab can write to the tmp/pids/ and tmp/sockets/ directories
    chmod -R u+rwX  tmp/{pids,sockets}

    # Make sure GitLab can write to the public/uploads/ directory
    chmod -R u+rwX  public/uploads

    # Copy the example Unicorn config
    sudo -u git -H cp config/unicorn.rb.example config/unicorn.rb

    # Enable cluster mode if you expect to have a high load instance
    # Ex. change amount of workers to 3 for 2GB RAM server
    sudo -u git -H editor config/unicorn.rb

    # Copy the example Rack attack config
    sudo -u git -H cp config/initializers/rack_attack.rb.example config/initializers/rack_attack.rb

    # Configure Git global settings for git user, useful when editing via web
    # Edit user.email according to what is set in config/gitlab.yml
    sudo -u git -H git config --global user.name "GitLab"
    sudo -u git -H git config --global user.email "gitlab@localhost"
    sudo -u git -H git config --global core.autocrlf input

**Important Note:**
Make sure to edit both `gitlab.yml` and `unicorn.rb` to match your setup.

### Configure GitLab DB settings

    # MySQL only:
    sudo -u git cp config/database.yml.mysql config/database.yml

    # MySQL and remote PostgreSQL only:
    # Update username/password in config/database.yml.
    # You only need to adapt the production settings (first part).
    # If you followed the database guide then please do as follows:
    # Change 'secure password' with the value you have given to $password
    # You can keep the double quotes around the password
    sudo -u git -H editor config/database.yml

    # PostgreSQL and MySQL:
    # Make config/database.yml readable to git only
    sudo -u git -H chmod o-rwx config/database.yml

### Install Gems

**Note:** As of bundler 1.5.2, you can invoke `bundle install -jN`
(where `N` the number of your processor cores) and enjoy the parallel gems installation with measurable
difference in completion time (~60% faster). Check the number of your cores with `nproc`.
For more information check this [post](http://robots.thoughtbot.com/parallel-gem-installing-using-bundler).
First make sure you have bundler >= 1.5.2 (run `bundle -v`) as it addresses some [issues](https://devcenter.heroku.com/changelog-items/411)
that were [fixed](https://github.com/bundler/bundler/pull/2817) in 1.5.2.

    cd /home/git/gitlab

    # For users from China mainland only
    nano /home/git/gitlab/Gemfile
    source "http://ruby.taobao.org" // instead of source "https://rubygems.org/"

    # For MySQL (note, the option says "without ... postgres")
    sudo -u git -H bundle install --deployment --without development test postgres aws

### Install GitLab shell

GitLab Shell is an ssh access and repository management software developed specially for GitLab.

```
# Go to the Gitlab installation folder:
cd /home/git/gitlab

# For users from China mainland only
nano /home/git/gitlab/Gemfile
source "http://ruby.taobao.org" // instead of source "https://rubygems.org/"

# Run the installation task for gitlab-shell (replace `REDIS_URL` if needed):
sudo -u git -H bundle exec rake gitlab:shell:install[v1.9.6] REDIS_URL=redis://localhost:6379 RAILS_ENV=production

# By default, the gitlab-shell config is generated from your main gitlab config.
#
# Note: When using GitLab with HTTPS please change the following:
# - Provide paths to the certificates under `ca_file` and `ca_path options.
# - The `gitlab_url` option must point to the https endpoint of GitLab.
# - In case you are using self signed certificate set `self_signed_cert` to `true`.
# See #using-https for all necessary details.
#
# You can review (and modify) it as follows:
sudo -u git -H editor /home/git/gitlab-shell/config.yml

# Ensure the correct SELinux contexts are set
# Read http://wiki.centos.org/HowTos/Network/SecuringSSH
restorecon -Rv /home/git/.ssh
```

### Initialize Database and Activate Advanced Features

    sudo -u git -H bundle exec rake gitlab:setup RAILS_ENV=production
    # Type 'yes' to create the database tables.
    # When done you see 'Administrator account created:'

**Note:** You can set the Administrator password by supplying it in environmental variable GITLAB_ROOT_PASSWORD, eg.:

    sudo -u git -H bundle exec rake gitlab:setup RAILS_ENV=production GITLAB_ROOT_PASSWORD=newpassword

### Install Init Script

Download the init script (will be /etc/init.d/gitlab):

    cp lib/support/init.d/gitlab /etc/init.d/gitlab
    chmod +x /etc/init.d/gitlab
    chkconfig --add gitlab

Make GitLab start on boot:

    chkconfig gitlab on

### Set up logrotate

    cp lib/support/logrotate/gitlab /etc/logrotate.d/gitlab

### Check Application Status

Check if GitLab and its environment are configured correctly:

    sudo -u git -H bundle exec rake gitlab:env:info RAILS_ENV=production

### Compile assets

    sudo -u git -H bundle exec rake assets:precompile RAILS_ENV=production

### Start your GitLab instance

    service gitlab start

## 6. Configure the web server

Use either Nginx or Apache, not both. Official installation guide recommends nginx.

### Nginx

Add nginx yum repository, create a file named /etc/yum.repos.d/nginx.repo and paste the configurations below:

    [nginx]
    name=nginx repo
    baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
    gpgcheck=0
    enabled=1

You will need a new version of nginx otherwise you might encounter an issue like [this][issue-nginx].
To do so, follow the instructions provided by the [nginx wiki][nginx-centos] and then install nginx with:

    yum update
    yum -y install nginx
    chkconfig nginx on
    cp lib/support/nginx/gitlab /etc/nginx/conf.d/gitlab.conf
    # If you are going to use https, use the command below instead.
    cp lib/support/nginx/gitlab-ssl /etc/nginx/conf.d/gitlab.conf

Edit `/etc/nginx/conf.d/gitlab.conf` and replace `YOUR_SERVER_FQDN` with your FQDN.

    # Change YOUR_SERVER_FQDN to the fully-qualified domain name of your host serving GitLab.
    editor /etc/nginx/sites-available/gitlab

Add `nginx` user to `git` group:

    usermod -a -G git nginx
    chmod g+rx /home/git/

Validate your gitlab or gitlab-ssl Nginx config file with the following command:

    nginx -t

Finally start nginx with:

    service nginx restart

## 7. Configure the firewall

By default, CentOS 7 use firewall as firewall, turn back to use iptables firewall.
### Turn off firewall

    systemctl stop firewalld.service
    systemctl disable firewalld.service

### Install ipatbles firewall

    # install iptables
    yum install iptables-services
    # edit iptables configurations
    vi /etc/sysconfig/iptables
    # add this line
    -A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
    # add this line if use https
    -A INPUT -m state --state NEW -m tcp -p tcp --dport 433 -j ACCEPT
    # save & exiq
    :wq!
    # restart iptables to take effect
    systemctl restart iptables.service
    # make iptables start on boot
    systemctl enable iptables.service

    lokkit -s http -s https -s ssh

Restart the service for the changes to take effect:

    service iptables restart


## Done!

### Double-check Application Status

To make sure you didn't miss anything run a more thorough check with:

    cd /home/git/gitlab
    sudo -u git -H bundle exec rake gitlab:check RAILS_ENV=production

If all items are green, then congratulations on successfully installing GitLab!
**NOTE:** Supply SANITIZE=true environment variable to gitlab:check to omit project names from the output of the check command.

## Initial Login

Visit YOUR_SERVER in your web browser for your first GitLab login.
The setup has created an admin account for you. You can use it to log in:

    root
    5iveL!fe (if you didn't supply it in environmental variable GITLAB_ROOT_PASSWORD)

**Important Note:**
Please go over to your profile page and immediately change the password, so
nobody can access your GitLab by using this login information later on.

**Enjoy!**

You can also check some [Advanced Setup Tips][tips].

## Links used in this guide

- [EPEL information](http://www.thegeekstuff.com/2012/06/enable-epel-repository/)
- [SELinux booleans](http://wiki.centos.org/TipsAndTricks/SelinuxBooleans)


[EPEL]: https://fedoraproject.org/wiki/EPEL
[PUIAS]: https://puias.math.ias.edu/wiki/YumRepositories6#Computational
[SDL]: https://puias.math.ias.edu
[PU]: http://www.princeton.edu/
[IAS]: http://www.ias.edu/
[keys]: https://fedoraproject.org/keys
[issue-nginx]: https://github.com/gitlabhq/gitlabhq/issues/5774
[nginx-centos]: http://wiki.nginx.org/Install#Official_Red_Hat.2FCentOS_packages
[psql-doc-auth]: http://www.postgresql.org/docs/9.3/static/auth-methods.html
[tips]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/install/installation.md#advanced-setup-tips